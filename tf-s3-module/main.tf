resource "random_integer" "this_random_vals" {
  min = 1000000
  max = 9999900
}

resource "aws_s3_bucket" "this_bucket" {
  bucket = "${var.bucket_name}-${random_integer.this_random_vals.id}"
  acl    = var.access_level
}
