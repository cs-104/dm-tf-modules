variable "aws_ami" {
  default = ""
}

variable "server_type" {
  default = ""
}

variable "target_keypairs" {
  default = ""
}

variable "acct_vpc_id" {
    default= ""
}

variable "acct_subnet_id" {
    default = ""
}

variable "env_name" {
    default = ""
}