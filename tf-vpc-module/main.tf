# Terraform workflow 
# 1. terraform version 
# 2. Set remote statefile storage(s3)
# 3. Establish connection with target account(s)
# 4. `terraform init` 
# 5. `terraform fmt`
# 6. `terraform validate`
# 7. `terraform plan` 
# 8. `terraform apply`
# 9. `terraform destroy`
# ******************************************************* #

# Define provider type AWS  
# provider "aws" {
#   region = "eu-west-1"
# }

# Configure backend for statefiles for these resources  
# terraform {
#   required_version = ">= 0.12.31" # Set minimum terraform version for this IAC scripts 

#   backend "s3" {                                     # Use s3 as the backend storage - Other storage option includes includes tf consul|
#     bucket  = "YOUR|BUCKET-NAME"                        # Use a target bucket on the given account    
#     key     = "lesson6/mini-vpc-/terraform.tfstates" # Create folder path to statefiles 
#     region  = "eu-west-1"                            # Configure bucket to be located in eu-west-1 
#     encrypt = "true"                                 # Encrypt data at rest - Menaning statefiles will be encrypted - Secured 
#   }
# }


data "aws_availability_zones" "this_ds_azs" {}
resource "random_integer" "this_random_vals" {
  min = 1000000
  max = 9999900
}

# ******************************************************* #
# Global variables 
# ******************************************************* #
variable "ip_range" {
  default = ""
}

variable "inst_tenancy" {
  default = ""
}

variable "dns_support" {
  default = ""
}

variable "dns_hostn" {
  default = ""
}

variable "which_env_nam_tag" {
  default = ""
}

variable "pub_ip_range" {
  description = "Specify public subnet ranges "
  type        = list
  default     = []
}

variable "pub_azs" {
  description = "Specify which public availability zones"
  type        = list
  default     = []
}


variable "priv_ip_range" {
  description = "Specify private ip ranges for private subnet"
  type        = list
  default     = []
}

variable "priv_azs" {
  description = "Specify which private availability zones for private subnet"
  type        = list
  default     = []
}

variable "enabled_nat_gateway" {
  description = "Create nat gateway for private repo"
  default     = "false"
  type        = string
}

variable "enabled_single_nat_gateway" {
  description = "Set to false if you are not creating any NATGs"
  default     = "false"
  type        = string
}


resource "aws_internet_gateway" "dm_gtw_res" {
  vpc_id = aws_vpc.dm_vpc_res.id
  tags = merge({
    "Name" = var.which_env_nam_tag
  })
}

# ******************************************************* #
# Resource definitions(CREATE)
# ******************************************************* #
resource "aws_vpc" "dm_vpc_res" {
  cidr_block           = var.ip_range
  instance_tenancy     = var.inst_tenancy
  enable_dns_support   = var.dns_support
  enable_dns_hostnames = var.dns_hostn

  tags = merge({
    "Name" = var.which_env_nam_tag
  })
}



# ******************************************************* #
# Public subnet definition
# Route
# ******************************************************* #
resource "aws_subnet" "dm_pub_res" {
  count                   = length(var.pub_ip_range)
  vpc_id                  = aws_vpc.dm_vpc_res.id
  cidr_block              = var.pub_ip_range[count.index]
  availability_zone       = var.pub_azs[count.index]
  map_public_ip_on_launch = true
  tags = merge({
    "Name" = format("%s-pub-%d", var.which_env_nam_tag, count.index)
  })
}

resource "aws_route" "dm_pub_rt_res" {
  route_table_id         = aws_route_table.dm_pub_rtt_res.id
  gateway_id             = aws_internet_gateway.dm_gtw_res.id
  destination_cidr_block = "0.0.0.0/0"
}

resource "aws_route_table" "dm_pub_rtt_res" {
  #   count  = length(var.pub_ip_range)
  vpc_id = aws_vpc.dm_vpc_res.id
  tags = merge({
    "Name" = format("%s-pub", var.which_env_nam_tag)
  })
}



resource "aws_route_table_association" "dm_pub_rt_assoc" {
  count          = length(var.pub_ip_range)
  subnet_id      = aws_subnet.dm_pub_res.*.id[count.index]
  route_table_id = aws_route_table.dm_pub_rtt_res.id


}

# ******************************************************* #
# Natgateways
# 
# ******************************************************* #
locals {
  nat_gateway_count = var.enabled_nat_gateway ? var.enabled_single_nat_gateway ? 1 : length(var.priv_ip_range) : 0
}


resource "aws_eip" "dm_eip_res" {
  count = local.nat_gateway_count
  vpc   = true
  tags = merge({
    "Name" = format("%s-eip-%d", var.which_env_nam_tag, count.index)
  })
  depends_on = [aws_internet_gateway.dm_gtw_res]
}

resource "aws_nat_gateway" "dm_natg_res" {
  count         = local.nat_gateway_count
  allocation_id = aws_eip.dm_eip_res.*.id[count.index]
  subnet_id     = aws_subnet.dm_pub_res.*.id[count.index]
  tags = merge({
    "Name" = format("%s-natg-%d", var.which_env_nam_tag, count.index)
  })
  depends_on = [aws_internet_gateway.dm_gtw_res]
}


# ******************************************************* #
# Private subnet definition
# Route
# ******************************************************* #
resource "aws_subnet" "dm_priv_res" {
  count                   = length(var.priv_ip_range)
  vpc_id                  = aws_vpc.dm_vpc_res.id
  cidr_block              = var.priv_ip_range[count.index]
  availability_zone       = var.priv_azs[count.index]
  map_public_ip_on_launch = false
  tags = merge({
    "Name" = format("%s-priv-%d", var.which_env_nam_tag, count.index)
  })
}

resource "aws_route_table" "dm_priv_rtt_res" {
  count  = length(var.priv_ip_range)
  vpc_id = aws_vpc.dm_vpc_res.id
  tags = merge({
    "Name" = format("%s-priv", var.which_env_nam_tag)
  })
}

resource "aws_route" "dm_priv_rt_res" {
  count                  = var.enabled_nat_gateway ? length(var.priv_ip_range) : 0
  nat_gateway_id         = var.enabled_single_nat_gateway ? aws_nat_gateway.dm_natg_res.*.id[0] : aws_nat_gateway.dm_natg_res.*.id[count.index]
  
  route_table_id         = aws_route_table.dm_priv_rtt_res.*.id[count.index]
  destination_cidr_block = "0.0.0.0/0"
}

resource "aws_route_table_association" "dm_priv_rt_assoc" {
  count          = length(var.priv_ip_range)
  subnet_id      = aws_subnet.dm_pub_res.*.id[count.index]
  route_table_id = aws_route_table.dm_pub_rtt_res.id
}

